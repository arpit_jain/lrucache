
PROJECT = "mycache"


all: install server


server : ;@echo "Starting ${PROJECT}....."; \
	export NODE_PATH=.; \
	coffee server.coffee 

install: ;@echo "Installing ${PROJECT}....."; \
	npm install

update: ;@echo "Updating ${PROJECT}....."; \
	git pull --rebase; \
	npm install

clean : ;
	rm -rf node_modules


.PHONY: server install clean update
