module.exports = (app) ->
  # Helpers
  app.helpers = require "#{__dirname}/../app/helpers"

  # Controllers
  app.helpers.autoload "#{__dirname}/../app/controllers", app
