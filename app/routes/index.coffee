module.exports = (app) ->
  # Index
  app.get '/get', app.ApplicationController.get
  
  app.get '/set', app.ApplicationController.set

  app.post '/get', app.ApplicationController.get
  
  app.post '/set', app.ApplicationController.set
  
  app.get '/keys', app.ApplicationController.keys
  
  app.post '/keys', app.ApplicationController.keys
  
  
  
  # Error handling (No previous route found. Assuming it’s a 404)
  app.get '/*', (req, res) ->
    NotFound res

  NotFound = (res) ->
    res.send "not found"
