fs = require 'fs'
async = require 'async'
Heap = require "heap"

CAPACITY = 5

# Recursively require a folder’s files
exports.autoload = autoload = (dir, app) ->
  fs.readdirSync(dir).forEach (file) ->
    return if file is ".DS_Store"
    path = "#{dir}/#{file}"
    stats = fs.lstatSync(path)

    # Go through the loop again if it is a directory
    if stats.isDirectory()
      autoload path, app
    else
      require(path)?(app)




exports.adjust_this_key = adjust_this_key = (accessed_keys, accessed_keys_map , keyval, key)->
    if not accessed_keys.head?
        accessed_keys.unshift key
        accessed_keys_map[key] = accessed_keys.head
        return
    
    if accessed_keys_map[key]?
        
        current = accessed_keys_map[key]
        if accessed_keys.head.value == current.value
            return
        if accessed_keys.tail.value == current.value
            popped = accessed_keys.pop()
            accessed_keys.unshift key
            accessed_keys_map[key] = accessed_keys.head
            new_tail = accessed_keys.tail
            accessed_keys_map[new_tail.value] = new_tail
            return
          
        prev = current.before
        next = current.after
        prev.after = next
        next.before = prev
        old_head = accessed_keys.head
        current.after = old_head
        old_head.before = current
        accessed_keys_map[old_head.value] = old_head
        current.before = null
        accessed_keys.head = current
        accessed_keys_map[key] = accessed_keys.head
        
    
    else
        accessed_keys.unshift key
        accessed_keys_map[key] = accessed_keys.head
        accessed_keys_map[accessed_keys.head.after.value] = accessed_keys.head.after
        
    if accessed_keys.length > CAPACITY
        old_tail_val = accessed_keys.tail.value
        accessed_keys.pop()
        new_tail_val = accessed_keys.tail.value
        accessed_keys[new_tail_val] = accessed_keys.tail
        delete accessed_keys_map[old_tail_val]
        delete keyval[old_tail_val]
        
    
          
    
exports.keys = (keyval, callback)->
    return callback null, Object.keys(keyval)
  

exports.get = (keyval, accessed_keys,accessed_keys_map, key)->
    if key and keyval[key]? 
        adjust_this_key accessed_keys, accessed_keys_map, keyval,  key
        return [null, keyval[key].value]
    return ["Does not exist", null]
  
exports.store = (keyval, accessed_keys, accessed_keys_map, raw_data)->
    try
        data = JSON.parse(raw_data)
    catch error
        return ["bad data", 0]
      
    key = data.key
    val = data.value

      
    if not(key? and val?)
      return  ["missing params", 0]
    
    ts = new Date().getTime()
    
    keyval[key] =  {"value":val}
    adjust_this_key accessed_keys, accessed_keys_map, keyval, key
        
    return  [null, 1]