fs = require "fs"
async = require "async"
helpers = require( 'app/helpers/index' )
{DoublyLinkedList} = require('doubly-linked-list')

# todo read from a source when server starts
try
    oldkeyval = fs.readFileSync "lrucachefile", 'ascii'
    oldaccessed = fs.readFileSync "lrucacheaccessed", 'ascii'
    keyval = JSON.parse olddata
catch error
    console.log "no history found initiating empty cache"
    keyval = {}

accessed_keys = new DoublyLinkedList()
accessed_keys_map = {}
  
module.exports = (app) ->
  class app.ApplicationController
    
      @set = (req, res) ->
        
          body  = req.query
          if Object.keys(req.body).length
              body = req.body
          
          key = body.key
          val = body.value
          
          [err, cbk] = helpers.store keyval, accessed_keys,accessed_keys_map, JSON.stringify({"key":key, "value":val})
          return res.json {"err":err, "result":cbk}
    
      @get = (req, res) ->
        
          key = req.query.key
          
          if Object.keys(req.body).length
              key = req.body.key
          
          
          [err, cbk] = helpers.get keyval, accessed_keys,accessed_keys_map, key
          return res.json {"error" : err, "result":cbk}

      
      @keys = (req, res) ->
          helpers.keys keyval, (err, cbk)->
              return res.json {"error" : err, "result":cbk}

