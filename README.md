Prototye for set and get of LRU cache. (using doubly linked list and an hash map)

For get and set method,  the complexity is  o(1) 

TODOs:

- Get rid of http and use: tcp  (http was used only to set and get using web browser for prototype )

- Try other alternatives of Node

- Make it persistant at some level.  (write to file ?  DB ? ) 

PS: this code just for a test prototype,  please do not judge.

To run the server: cd lrucache \ make

the code is currently deployed on 52.24.7.40 (NO LONGER AVAILABLE NOW)

API:

set:   sample post request data: {"key":"key1", "value":"val1"}

get:   sample post request data: {"key":"key1"}

also added a functionality of "keys":  to get all the keys in cache

Usage:

```
arpit@arpit-Lenovo-Z51-70:~$ curl -H "Content-Type: application/json" -X POST -d '{"key":"key1", "value":"val1"}'  "http://52.24.7.40/set"
{
  "err": null,
  "result": 1
}

arpit@arpit-Lenovo-Z51-70:~$ curl -H "Content-Type: application/json" -X POST -d '{"key":"key1"}'  "http://52.24.7.40/get"
{
  "error": null,
  "result": "val1"
}

arpit@arpit-Lenovo-Z51-70:~$ curl   "http://52.24.7.40/keys"
{
  "error": null,
  "result": [
    "key1"
  ]
}
```

to test the code:
```
cd lrucache
npm install 
coffee cachetest.coffee
```